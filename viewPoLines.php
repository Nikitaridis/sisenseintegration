<?php

$sql = null;



try {
    $conn = mysqli_connect("lowcarbon-dev.datore.eu", "halvan", "UWSE&*#$^*&#%^#%", "datore_ericwright");
    $query = "select pl.direct_unit_cost lineAmount,\n" .
        "pl.description,\n" .
        "pl.po_number,\n" .
        "pl.buy_from_vendor_no as subcontractorId,\n" .
        "po.user_id as userId,\n" .
        "po.pay_to_address as subcontractorAddress1,\n" .
        "po.po_date as poDate,\n".
        "po.pay_to_address_two as subcontractorAddress2,\n" .
        "po.buy_from_vendor_no as supplierId,\n" .
        "po.comments as comments\n" .
        "from purchase_line pl, purchase_orders po where pl.po_number = po.po_number and po.po_number='". $_GET['orderId'] ."' and po.po_date like '2020-03-03%'";
    $sql = mysqli_query($conn, $query);

    if (!($conn->query($sql) === TRUE)) {
        echo "Database" . $conn->error;
    }
} catch (Exception $e) {
    print_r("Exception");
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
    print_r($e->getTrace());
}


echo "PO Number: " . $_GET['orderId'];
?>


<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Purchase Order Lines</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
</head>
<body>
<br/>
<div class="container">
    <h2 align="center">Purchase Order Lines</h2>
    <br/>
    <br/>
    <br/>
    <div id="purchase_order">
        <table class="table table-bordered">
            <tr>
                <th width=8%>Line Amount</th>
                <th width=8%>PO Date</th>
                <th width=8%>Description</th>
                <th width=8%>PO Number</th>
                <th width=8%>Subcontractor ID</th>
                <th width=8%>User ID</th>
                <th width=8%>subcontractor Address1</th>
                <th width=8%>subcontractor Address2</th>
                <th width=8%>Supplier ID</th>
                <th width=8%>Comments</th>
            </tr>
            <?php
            try {

                while ($row = mysqli_fetch_array($sql)) {
                    ?>
                    <tr>
                        <td width=8%><?php echo "£ ".round($row["lineAmount"],2); ?></td>
                        <td width=8%><?php echo substr($row["poDate"],0,10); ?></td>
                        <td width=8%><?php echo $row["description"]; ?></td>
                        <td width=8%><?php echo $row["po_number"]; ?></td>
                        <td width=8%><?php echo $row["subcontractorId"]; ?></td>
                        <td width=8%><?php echo $row["userId"]; ?></td>
                        <td width=8%><?php echo $row["subcontractorAddress1"]; ?></td>
                        <td width=8%><?php echo $row["subcontractorAddress2"]; ?></td>
                        <td width=8%><?php echo $row["supplierId"]; ?></td>
                        <td width=8%><?php echo $row["comments"]; ?></td>
                    </tr>
                    <?php
                }
            }catch (Exception $e2) {
                print_r("Exception2");
                print_r($e2->getMessage());
                print_r($e2->getTraceAsString());
                print_r($e2->getTrace());
            }
            ?>
        </table>
        <a href="ViewPurchaseOrders.php">Back to View Purchase Orders</a><br/>
        <a href="index.html">Back to index</a>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
</body>
</html>