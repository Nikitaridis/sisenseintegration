<?php
$conn = mysqli_connect("lowcarbon-dev.datore.eu", "halvan", "UWSE&*#$^*&#%^#%", "datore_ericwright");
$query = "select\n".
    "    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number limit 1 )as siteCode,\n".
    "    po.po_date as date,\n".
    "    po.po_number as poNumber,\n" .
    "    po.supplier as supplier,\n" .
    "    po.supplier as subcontractor,\n" .
    "    po.ship_to_address as siteAddress1,\n" .
    "    po.ship_to_address_two as siteAddress2,\n" .
    "    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,\n" .
    "    po.grn_date as grnDate,\n" .
    "    po.grn_number as grnNumber,\n" .
    "    po.comments as comments\n" .
    "    from purchase_orders po where po.status='yes' and length(po.grn_number) > 0";

$sql=null;
try {
    $sql = mysqli_query($conn, $query);

    if (!($conn->query($sql) === TRUE)) {
        echo "Database: " . $conn->error;
    }

} catch(Exception $e) {
    print_r("Exception");
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
    print_r($e->getTrace());
}

?>
<!--GRANT ALL PRIVILEGES ON *.* TO 'halvan'@'%' identified by 'UWSE&*#$^*&#%^#%';-->

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Approved Purchase Orders</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
</head>
<body>
    <br/>
    <div class="container">
        <h2 align="center">Approved Purchase Orders</h2>
        <br/>
        <br/>
        <br/>
        <div id="purchase_order">
            <table class="table table-bordered">
                <tr>
                    <th width=8%>Site Code</th>
                    <th width=8%>PO Number</th>
                    <th width=8%>Date</th>
                    <th width=8%>Subcontractor</th>
                    <th width=8%>Site Name</th>
                    <th width=8%>Site Address</th>
                    <th width=8%>Site Address 2</th>
                    <th width=8%>Total Amount</th>
                    <th width=8%>GRN Number</th>
                    <th width=8%>GRN Date</th>
                    <th width=8%>Comments</th>
                </tr>
                <?php

                while($row= mysqli_fetch_array($sql))
                {
                    $grnDate = null;
                    if(strlen($row["grnNumber"])>1) {
                        $grnDate = $row["grnDate"];
                    }
                    ?>
                    <tr>
                        <td width=8% ><?php echo $row["siteCode"]; ?></td>
                        <td width=8%><a href="viewPoLines.php?orderId=<?php echo $row["poNumber"]; ?>"><?php echo $row["poNumber"]; ?></a></td>
                        <td width=8%><?php echo substr($row["date"],0,10); ?></td>
                        <td width=8%><?php echo $row["subcontractor"]; ?></td>
                        <td width=8%><?php echo $row["supplier"]; ?></td>
                        <td width=8%><?php echo $row["siteAddress1"]; ?></td>
                        <td width=8%><?php echo $row["siteAddress2"]; ?></td>
                        <td width=8%><?php echo "£ ".round($row["totalAmount"],2); ?></td>
                        <td width=8%><?php echo $row["grnNumber"]; ?></td>
                        <td width=8%><?php echo $grnDate; ?></td>
                        <td width=8%><?php echo $row["comments"]; ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
    <a href="index.html">Back to index</a></br>
    <a href="ViewCheckedPurchaseOrders.php">Back to Checked purchase orders</a>
</body>
</html>