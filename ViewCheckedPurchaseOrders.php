<?php ?>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">

    <p>Date format must currently be in format yyyy-mm-dd or yyyy/mm/dd</p>
    Start Date: <input type="text" name="startDate">
    End Date: <input type="text" name="endDate">
    PO Number: <input type="text" name="poNumber">
    <input type="submit">
</form>
<?php

$conn = mysqli_connect("ew.datoredata.com", "halvan", "UWSE&*#$^*&#%^#%", "datore_ericwright");


if(!empty($_POST['poNumber'])) {

    $query = "select\n".
        "    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number and length(a.shortcut_dimension_code) > 1 limit 1 )as siteCode,\n".
        "    po.po_date as date,\n".
        "    po.id as id,\n".
        "    po.po_number as poNumber,\n" .
        "    po.supplier as subcontractor,\n" .
        "    po.site as site,\n" .
        "    po.ship_to_address as siteAddress1,\n" .
        "    po.ship_to_address_two as siteAddress2,\n" .
        "    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,\n" .
        "    po.grn_date as grnDate,\n" .
        "    po.grn_number as grnNumber\n" .
        "    from purchase_orders po where po.checked = 'yes' and po.status = 'no' and length(po.grn_number) > 1 and po.po_number='".$_POST['poNumber']."'";
}


if(!empty($_POST['startDate']) && (!empty($_POST['endDate']))) {
    $query = "select\n".
        "    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number limit 1 )as siteCode,\n".
        "    po.po_date as date,\n".
        "    po.po_number as poNumber,\n" .
        "    po.id as id,\n".
        "    po.supplier as subcontractor,\n" .
        "    po.site as site,\n" .
        "    po.ship_to_address as siteAddress1,\n" .
        "    po.ship_to_address_two as siteAddress2,\n" .
        "    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,\n" .
        "    po.grn_date as grnDate,\n" .
        "    po.grn_number as grnNumber,\n" .
        "    po.comments as comments\n" .
        "    from purchase_orders po where po.checked = 'yes' and po.status = 'no' and length(po.grn_number) > 1 and po.po_date between '". $_POST['startDate'] ."' and '". $_POST['endDate'] ."'having totalAmount > 500";

}

if(empty($_POST['startDate']) && empty($_POST['endDate']) && empty($_POST['poNumber'])) {
    $query = "select\n".
        "    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number limit 1 )as siteCode,\n".
        "    po.po_date as date,\n".
        "    po.po_number as poNumber,\n" .
        "    po.id as id,\n".
        "    po.supplier as supplier,\n" .
        "    po.supplier as subcontractor,\n" .
        "    po.ship_to_address as siteAddress1,\n" .
        "    po.ship_to_address_two as siteAddress2,\n" .
        "    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,\n" .
        "    po.grn_date as grnDate,\n" .
        "    po.grn_number as grnNumber,\n" .
        "    po.comments as comments\n" .
        "    from purchase_orders po where po.checked = 'yes' and po.status = 'no' and length(po.grn_number) > 1 having totalAmount > 500";

}


$sql=null;
try {
    $sql = mysqli_query($conn, $query);

    if (!($conn->query($sql) === TRUE)) {
        echo "Database: " . $conn->error;
    }

} catch(Exception $e) {
    print_r("Exception");
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
    print_r($e->getTrace());
}

?>
<!--GRANT ALL PRIVILEGES ON *.* TO 'halvan'@'%' identified by 'UWSE&*#$^*&#%^#%';-->

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Checked Purchase Orders</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
</head>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <br/>
    <div class="container">
        <h2 align="center">Checked Purchase Orders</h2>
        <br/>
        <br/>
        <br/>
        <div id="purchase_order">
            <table class="table table-bordered">
                <tr>
                    <th width=8%>Site Code</th>
                    <th width=8%>PO Number</th>
                    <th width=8%>Date</th>
                    <th width=8%>Subcontractor</th>
                    <th width=8%>Site Name</th>
                    <th width=8%>Site Address</th>
                    <th width=8%>Site Address 2</th>
                    <th width=8%>Total Amount</th>
                    <th width=8%>GRN Number</th>
                    <th width=8%>GRN Date</th>
                    <th width=8%>Comments</th>
                    <th width=8%>Approve PO</th>
                    <th width=8%>Enter new comment</th>
                </tr>
                <?php

                while($row= mysqli_fetch_array($sql))
                {
                    $grnDate = null;
                    if(strlen($row["grnNumber"])>1) {
                        $grnDate = $row["grnDate"];
                    }
                    ?>
                    <tr>
                        <td width=8% ><?php echo $row["siteCode"]; ?></td>
                        <td width=8%><a href="viewPoLines.php?orderId=<?php echo $row["poNumber"]; ?>"><?php echo $row["poNumber"]; ?></a></td>
                        <td width=8%><?php echo substr($row["date"],0,10); ?></td>
                        <td width=8%><?php echo $row["subcontractor"]; ?></td>
                        <td width=8%><?php echo $row["supplier"]; ?></td>
                        <td width=8%><?php echo $row["siteAddress1"]; ?></td>
                        <td width=8%><?php echo $row["siteAddress2"]; ?></td>
                        <td width=8%><?php echo "£ ".round($row["totalAmount"],2); ?></td>
                        <td width=8%><?php echo $row["grnNumber"]; ?></td>
                        <td width=8%><?php echo substr($grnDate,0,10); ?></td>
                        <td width=8%><?php echo $row["comments"]; ?></td>
                        <td width=8%><input name='checkboxes[]' type="checkbox" value = "<?php echo $row["poNumber"]; ?>"> </td>
                        <td width=8%><textarea name='comments[<?php echo $row["id"]; ?>]'></textarea></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
    <input type="submit" value="Submit" name="Submit"/>
</form>
<?php
if(isset($_POST['Submit'])){
    // print_r($_POST['checkboxes']);

    if(!empty($_POST['checkboxes'])) {
        $conn = mysqli_connect("localhost", "halvan", "UWSE&*#$^*&#%^#%", "datore_ericwright");

        foreach($_POST['checkboxes'] as $value){
            // $sqlUpdateStatus = "update purchase_orders set status = 'yes' where po_number = '$value'";
            $sqlUpdateStatus = "update purchase_orders set status = 'yes' where po_number = '$value'";
            if ($conn->query($sqlUpdateStatus) === TRUE) {
                echo "updated PO approval with id ".$value.'<br/>';
            } else {
                echo "Error updating record: " . $conn->error;
            }
        }
        $conn->close();

    }
    if(!empty($_POST['comments'])) {
        // print_r($_POST['comments']);
        $conn = mysqli_connect("localhost", "halvan", "UWSE&*#$^*&#%^#%", "datore_ericwright");

        foreach($_POST['comments'] as $value){
            if(strlen($value)> 1) {
                $elementId = array_search($value, $_POST['comments']);
                $sqlUpdateStatus = "update purchase_orders set comments = '$value' where id = '$elementId'";
                if ($conn->query($sqlUpdateStatus) === TRUE) {
                    echo "updated DB comment for PO with id ".$elementId.'<br/>';
                } else {
                    echo "Error updating record: " . $conn->error;
                }
            }
        }
        $conn->close();
    }
}
?>

<a href="index.html">Back to index</a>
</body>
</html>