select
    pl.shortcut_dimension_code as siteCode,
    po.po_date as date,
    po.po_number  poNumber,
    pl.buy_from_vendor_no  subcontractorId,
    po.pay_to_address  subcontractorAddress1,
    po.pay_to_address_two  subcontractorAddress2,
    po.supplier  supplier,
    po.ship_to_address  siteAddress1,
    po.ship_to_address_two  siteAddress2,
    pl.description  description,
    po.amount  amount,
    po.grn_date  grnDate,
    po.grn_number  grnNumber,
    po.user_id  userId
from purchase_line pl,purchase_orders po where po.po_number = pl.po_number;

select * from purchase_line where length(shortcut_dimension_code) > 0;

select * from purchase_orders order by purchase_orders.po_number desc;

select * from purchase_orders where length(grn_number)=0 and grn_date is not null;

select count(*) as total,po_number from purchase_orders group by po_number  having total >1


select
    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number limit 1 )as siteCode,
    po.po_date as date,
    po.po_number as poNumber,
    po.supplier as supplier,
    po.supplier as subcontractor,
    po.ship_to_address as siteAddress1,
    po.ship_to_address_two as siteAddress2,
    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,
    po.grn_date as grnDate,
    po.grn_number as grnNumber
from purchase_line pl,purchase_orders po  limit 50;


select * from purchase_orders where po_number = 'PO/FM/105193';
select * from purchase_line where po_number = 'PO/FM/103779';

select sum(direct_unit_cost) as total,po_number from purchase_line group by po_number;

select pl.direct_unit_cost lineAmount,
       pl.description,
       pl.po_number,
       pl.shortcut_dimension_code as siteId,
       pl.buy_from_vendor_no as subcontractorId,
       po.user_id as userId,
       po.pay_to_address as subcontractorAddress1,
       po.pay_to_address_two as subcontractorAddress2,
       po.buy_from_vendor_no as supplierId,
       po.comments as comments

from purchase_line pl, purchase_orders po where pl.po_number = po.po_number and po.po_number='PO/FM/103778';


select * from purchase_orders where status = 'yes';


select * from purchase_orders where po_number = 'PLT/FM/10006';
select * From purchase_line where po_number = 'PLT/FM/10097';
select count(*) as count,po_number from purchase_orders group by po_number having count > 1;


update purchase_orders set id = (select floor(rand()*10000)) where 1 = 1;
update purchase_line b set b.id = (select a.id from purchase_orders a where a.po_number = b.po_number) where 1=1;



select
    (select a.shortcut_dimension_code from purchase_line a where a.po_number = po.po_number limit 1 )as siteCode,
    po.po_date as date,
    po.po_number as poNumber,
    po.supplier as supplier,
    po.supplier as subcontractor,
    po.ship_to_address as siteAddress1,
    po.ship_to_address_two as siteAddress2,
    (select sum(pll.direct_unit_cost) from purchase_line pll where pll.po_number = po.po_number group by pll.po_number) as totalAmount,
    po.grn_date as grnDate,
    po.grn_number as grnNumber,
    po.comments as comments
from purchase_line pl,purchase_orders po where po.status='no';



select * from purchase_orders where po_date like '2020-03-03%';
create index poNumberIdx on purchase_orders(po_date);
create index poNumberIdx2 on purchase_orders(po_number);
create index poNumberIdx3 on purchase_orders(status);
create index poNumberIdx4 on purchase_orders(checked);


create index po_number_idx on purchase_line(po_number);
create index shortcut_dimension_code_idx on purchase_line(shortcut_dimension_code);



-- sort out the date issue in the DB
-- MySQL's default DATE field format is YYYY-MM-DD
select po_date,date_format(str_to_date(po_date,'%m/%d/%Y'),'%d-%m-%Y') from purchase_line;
select po_date,date_format(str_to_date(po_date,'%d-%m-%Y'),'%Y-%m-%d') from purchase_line;

update purchase_line set po_date = date_format(str_to_date(po_date,'%m/%d/%Y'),'%d-%m-%Y') where 1 = 1 ;
update purchase_line set po_date = date_format(str_to_date(po_date,'%d-%m-%Y'),'%Y-%m-%d') where 1 = 1 ;


-- do not need to change the format of the date in purchase_orders_table , just change col of GRN date
update purchase_orders set po_date = date_format(str_to_date(po_date,'%Y/%m/%d %T'),'%Y-%m-%d %T') where 1 = 1



